# Unicorn-Happybot
🦄
## About

Unicorn-Happybot (Currently identified by User-Agent: Unicorn-Happybot/v1.0) is our web crawler. We aim for him to be as polite as possible when exploring the web. Crawling the web is one way Unicorn is helping us to discover whatever information we can find to indicate areas where there are social issues or community health problems.

## Politeness

Unicorn will honor any robots.txt directives. He does not want to intrude and he does not want to burn through your resources.

If you feel like Unicorn has caused you problems and want to tell us about it, please contact us on:
abuse@unicorn-happybot.uk

## Additional Information

Unicorn is a unicorn and he is happy. 

[https://skydrops.gitlab.io/unicorn-happybot/](https://skydrops.gitlab.io/unicorn-happybot/)
